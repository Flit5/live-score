const fastify = require('fastify')({ logger: true })

// Import DB
const db = require('./login')
fastify.register(db)

// Register Postgres
require('dotenv').config()
fastify.register(require('@fastify/postgres'), {
    connectionString: `postgres://${process.env.PG_USER}:${process.env.PG_PASSWORD}@${process.env.PG_HOST}:${process.env.PG_PORT}/${process.env.PG_DB}`
})

// Connect Postgres
const { Client } = require('pg')
const client = new Client({
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    database: process.env.PG_DB
})
client.connect(err => {
    if(err) {
        console.log(err.message)
    } else {
        console.log('Connected')
    }
})

// Route Testing
fastify.get('/', (req, res) => {
    res.send({hello: 'this is test'})
})

// Server
fastify.listen({ port: process.env.PORT, host: '0.0.0.0'}, err => {
    if(err) throw err
    console.log(`Server Listening on ${fastify.server.address().port}`)
})

module.exports = client