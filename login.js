const client = require('./server')
const fp = require('fastify-plugin')
const { v4: uuidv4 } = require('uuid')
const id = uuidv4

module.exports = async function db (fastify,opts) {
    // Get All Users
    fastify.get('/users', async (req, reply) => {
        const client = await fastify.pg.connect()
        try {
            const { rows } = await client.query(`SELECT * FROM users`)
            return rows
        } finally {
            client.release()
        }
    })
    // Get User
    fastify.get('/users/:id', async (req, reply) => {
        const client = await fastify.pg.connect()
        const id = req.params.id

        try {
            const { rows } = await client.query(`SELECT * FROM users WHERE id=$1`, 
            [id])
            return rows
        } finally {
            client.release()
        }
    })
    // Create User
    fastify.post('/users', async (req, reply) => {
        const client = await fastify.pg.connect()
        const { name, email, password, level } = req.body

        try {
            const { rows } = await client.query(`INSERT INTO users (name, email, password, level) VALUES ($1, $2, $3, $4)`, 
            [name, email, password, level])

            console.log(rows[0])
            reply.send('User has been created!')
        } finally {
            client.release()
        }
    })
    // Update User
    fastify.put('/users/:id', async (req, reply) => {
        const client = await fastify.pg.connect()
        const id = req.params.id
        const { name, email, password, level } = req.body

        try {
            const { rows } = await client.query(`UPDATE users SET name=$1, email=$2, password=$3, level=$4 WHERE id=$5`, 
            [name, email, password, level, id])

            console.log(rows[0])
            reply.send(`User ${id} has been updated!`)
        } finally {
            client.release()
        }
    })
    // Delete User
    fastify.delete('/users/:id', async (req, reply) => {
        const client = await fastify.pg.connect()
        const id = req.params.id

        try {
            const { rows } = await client.query(`DELETE FROM users WHERE id=$1`, 
            [id])

            console.log(rows[0])
            reply.send(`User ${id} has been deleted!`)
        } finally {
            client.release()
        }
    })
}