FROM node:alpine

WORKDIR /app

COPY . .

CMD [ "npm", "start" ]

EXPOSE 5070